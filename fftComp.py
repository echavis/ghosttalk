import scipy
from scipy.io import wavfile
import scipy.fftpack
import matplotlib.pyplot as plt
from pylab import *
import sys
import math
import numpy

def produceFFT(fileName):
    sampRate, readings = wavfile.read(fileName)
    print len(readings)
    normalized = [(elt/2**16)*2-1 for elt in readings]
    print fileName, 'readings', readings[1000:1010]
    print fileName, 'normalized', normalized[1000:1010]
    ffted = scipy.fftpack.rfft(normalized)
    return ffted

def saveFFT(ffted, fileName):
    halfLen = len(ffted)/2
    plt.ylabel("power")
    plt.xlabel("frequency")
    plt.axis([0, 20000, 0, 10000])
    plt.plot(abs(ffted[:halfLen-1]))
    savefig(fileName+'.png', bbox_inches="tight")

def magnitude(cpx):
    return sqrt( pow(cpx.real,2) + pow(cpx.imag, 2) )

def compFFTs(fft1, fft2):
    step = 44100.0/len(fft1)
    freq = 0

def invert(fName):
    rate, data = wavfile.read(fName)
    ffted = scipy.fftpack.rfft(data)
    unffted = scipy.fftpack.irfft(ffted)
    nonComplex = numpy.round(unffted).astype('int16')
    return nonComplex
   
def findHigh(ffted, num):
    freqs = []
    numPoints = len(ffted)
    rate = 44100.0
    step = rate/numPoints
    print 'step: ', step
    freq = 0
    for p in ffted:
        if p > num:
            freqs.append(freq)
        freq += step
    return freqs

#the two files must be of the same length
def adaptFile(fName1, fName2, high):
    fft1 = produceFFT(fName1)
    fft2 = produceFFT(fName2)
    highFreqs = findHigh(fft1, high)
    numPoints = len(fft2)
    step = 44100.0/numPoints
    freq = 0
    numSteps = 0
    fft2 = [0.0]*len(fft2)
    '''
    while freq < 10000:
        if freq > 5500 and freq < 10000:
            print 'len', len(fft2) 
            print 'numSteps', numSteps
            print 'pre', fft2[numSteps]
            fft2[numSteps] = fft2[numSteps] * 100
            print 'post', fft2[numSteps]
        freq = freq + step
        numSteps += 1
    '''
    #print fft2
    unffted = scipy.fftpack.irfft(fft2)           
    return unffted

def main(args):
    #invertedTwice = invert(args[1])
    #wavfile.write("z.wav", 44100, invertedTwice) 
    adapted = adaptFile(args[1], args[2], 5000) 
    fft1 = produceFFT(args[1])
    saveFFT(fft1,args[1])
    saveFFT(adapted,args[2])
    '''
    fft1 = produceFFT(args[1])
    fft2 = produceFFT(args[2])
    over5000s1 = findPlus5000(fft1, 5000)
    over5000s2 = findPlus5000(fft2, 5000)
    print "len1: ", len(over5000s1)
    print over5000s1
    print "len2: ", len(over5000s2)
    saveFFT(fft1,args[1])
    saveFFT(fft2,args[2])
    compFFTs(fft1, fft2)
    '''

if __name__ == '__main__' : main(sys.argv) 




