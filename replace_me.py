#!/usr/bin/env python2
##################################################
# GNU Radio Python Flow Graph
# Title: Replace Me
# Generated: Sat Nov  7 23:31:54 2015
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import uhd
from gnuradio import wxgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from gnuradio.wxgui import forms
from gnuradio.wxgui import scopesink2
from gnuradio.wxgui import waterfallsink2
from grc_gnuradio import blks2 as grc_blks2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import time
import wx

class replace_me(grc_wxgui.top_block_gui):

    def __init__(self):
        grc_wxgui.top_block_gui.__init__(self, title="Replace Me")

        ##################################################
        # Variables
        ##################################################
        self.transmit_power = transmit_power = 26
        self.samp_rate = samp_rate = 250000
        self.psk_1on_0off = psk_1on_0off = 0
        self.modulated_signal_amplitude = modulated_signal_amplitude = .5
        self.modulated_freq = modulated_freq = 500
        self.fsk_1on_0off = fsk_1on_0off = 0
        self.carrier_freq = carrier_freq = 900000000
        self.am_1on_0off = am_1on_0off = 0

        ##################################################
        # Blocks
        ##################################################
        _transmit_power_sizer = wx.BoxSizer(wx.VERTICAL)
        self._transmit_power_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_transmit_power_sizer,
        	value=self.transmit_power,
        	callback=self.set_transmit_power,
        	label='transmit_power',
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._transmit_power_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_transmit_power_sizer,
        	value=self.transmit_power,
        	callback=self.set_transmit_power,
        	minimum=0,
        	maximum=31,
        	num_steps=32,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_transmit_power_sizer)
        _psk_1on_0off_sizer = wx.BoxSizer(wx.VERTICAL)
        self._psk_1on_0off_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_psk_1on_0off_sizer,
        	value=self.psk_1on_0off,
        	callback=self.set_psk_1on_0off,
        	label='psk_1on_0off',
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._psk_1on_0off_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_psk_1on_0off_sizer,
        	value=self.psk_1on_0off,
        	callback=self.set_psk_1on_0off,
        	minimum=0,
        	maximum=1,
        	num_steps=1,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_psk_1on_0off_sizer)
        _modulated_signal_amplitude_sizer = wx.BoxSizer(wx.VERTICAL)
        self._modulated_signal_amplitude_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_modulated_signal_amplitude_sizer,
        	value=self.modulated_signal_amplitude,
        	callback=self.set_modulated_signal_amplitude,
        	label="modulated_signal_amplitude",
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._modulated_signal_amplitude_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_modulated_signal_amplitude_sizer,
        	value=self.modulated_signal_amplitude,
        	callback=self.set_modulated_signal_amplitude,
        	minimum=0,
        	maximum=1,
        	num_steps=2,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_modulated_signal_amplitude_sizer)
        _fsk_1on_0off_sizer = wx.BoxSizer(wx.VERTICAL)
        self._fsk_1on_0off_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_fsk_1on_0off_sizer,
        	value=self.fsk_1on_0off,
        	callback=self.set_fsk_1on_0off,
        	label='fsk_1on_0off',
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._fsk_1on_0off_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_fsk_1on_0off_sizer,
        	value=self.fsk_1on_0off,
        	callback=self.set_fsk_1on_0off,
        	minimum=0,
        	maximum=1,
        	num_steps=1,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_fsk_1on_0off_sizer)
        _carrier_freq_sizer = wx.BoxSizer(wx.VERTICAL)
        self._carrier_freq_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_carrier_freq_sizer,
        	value=self.carrier_freq,
        	callback=self.set_carrier_freq,
        	label='carrier_freq',
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._carrier_freq_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_carrier_freq_sizer,
        	value=self.carrier_freq,
        	callback=self.set_carrier_freq,
        	minimum=600000000,
        	maximum=1200000000,
        	num_steps=100,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_carrier_freq_sizer)
        _am_1on_0off_sizer = wx.BoxSizer(wx.VERTICAL)
        self._am_1on_0off_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_am_1on_0off_sizer,
        	value=self.am_1on_0off,
        	callback=self.set_am_1on_0off,
        	label='am_1on_0off',
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._am_1on_0off_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_am_1on_0off_sizer,
        	value=self.am_1on_0off,
        	callback=self.set_am_1on_0off,
        	minimum=0,
        	maximum=1,
        	num_steps=1,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_am_1on_0off_sizer)
        self.wxgui_waterfallsink2_0 = waterfallsink2.waterfall_sink_c(
        	self.GetWin(),
        	baseband_freq=0,
        	dynamic_range=100,
        	ref_level=0,
        	ref_scale=2.0,
        	sample_rate=samp_rate,
        	fft_size=512,
        	fft_rate=15,
        	average=False,
        	avg_alpha=None,
        	title="Waterfall Plot",
        )
        self.Add(self.wxgui_waterfallsink2_0.win)
        self.wxgui_scopesink2_0 = scopesink2.scope_sink_c(
        	self.GetWin(),
        	title="Scope Plot",
        	sample_rate=samp_rate,
        	v_scale=0,
        	v_offset=0,
        	t_scale=0,
        	ac_couple=False,
        	xy_mode=False,
        	num_inputs=1,
        	trig_mode=wxgui.TRIG_MODE_AUTO,
        	y_axis_label="Counts",
        )
        self.Add(self.wxgui_scopesink2_0.win)
        self.uhd_usrp_source_0 = uhd.usrp_source(
        	",".join(("", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_center_freq(carrier_freq, 0)
        self.uhd_usrp_source_0.set_gain(15, 0)
        self.uhd_usrp_source_0.set_antenna("RX2", 0)
        self.uhd_usrp_source_0.set_bandwidth(samp_rate, 0)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join(("", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_center_freq(carrier_freq, 0)
        self.uhd_usrp_sink_0.set_gain(transmit_power, 0)
        self.uhd_usrp_sink_0.set_antenna("TX/RX", 0)
        self.rational_resampler_xxx_0 = filter.rational_resampler_fff(
                interpolation=250000,
                decimation=44100,
                taps=None,
                fractional_bw=None,
        )
        _modulated_freq_sizer = wx.BoxSizer(wx.VERTICAL)
        self._modulated_freq_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_modulated_freq_sizer,
        	value=self.modulated_freq,
        	callback=self.set_modulated_freq,
        	label='modulated_freq',
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._modulated_freq_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_modulated_freq_sizer,
        	value=self.modulated_freq,
        	callback=self.set_modulated_freq,
        	minimum=0,
        	maximum=20000,
        	num_steps=1000,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_modulated_freq_sizer)
        self.digital_gfsk_mod_0 = digital.gfsk_mod(
        	samples_per_symbol=16,
        	sensitivity=1.0,
        	bt=0.35,
        	verbose=False,
        	log=False,
        )
        self.digital_dxpsk_mod_0 = digital.dbpsk_mod(
        	samples_per_symbol=16,
        	excess_bw=0.35,
        	mod_code="gray",
        	verbose=False,
        	log=False)
        	
        self.blocks_multiply_const_vxx_4_0 = blocks.multiply_const_vcc((fsk_1on_0off, ))
        self.blocks_multiply_const_vxx_4 = blocks.multiply_const_vcc((psk_1on_0off, ))
        self.blocks_multiply_const_vxx_3 = blocks.multiply_const_vcc((modulated_signal_amplitude, ))
        self.blocks_multiply_const_vxx_2 = blocks.multiply_const_vcc((modulated_signal_amplitude, ))
        self.blocks_multiply_const_vxx_1 = blocks.multiply_const_vff((am_1on_0off, ))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vff((modulated_signal_amplitude, ))
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_add_xx_1 = blocks.add_vcc(1)
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        self.blocks_add_const_vxx_0 = blocks.add_const_vff((.5, ))
        self.blks2_packet_encoder_0_0 = grc_blks2.packet_mod_f(grc_blks2.packet_encoder(
        		samples_per_symbol=16,
        		bits_per_symbol=8,
        		preamble="",
        		access_code="",
        		pad_for_usrp=True,
        	),
        	payload_length=0,
        )
        self.blks2_packet_encoder_0 = grc_blks2.packet_mod_f(grc_blks2.packet_encoder(
        		samples_per_symbol=16,
        		bits_per_symbol=8,
        		preamble="",
        		access_code="",
        		pad_for_usrp=True,
        	),
        	payload_length=0,
        )
        self.beethovens_5th = blocks.wavfile_source("y.wav", True)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.beethovens_5th, 0), (self.blks2_packet_encoder_0, 0))    
        self.connect((self.beethovens_5th, 0), (self.blks2_packet_encoder_0_0, 0))    
        self.connect((self.beethovens_5th, 0), (self.rational_resampler_xxx_0, 0))    
        self.connect((self.blks2_packet_encoder_0, 0), (self.digital_dxpsk_mod_0, 0))    
        self.connect((self.blks2_packet_encoder_0_0, 0), (self.digital_gfsk_mod_0, 0))    
        self.connect((self.blocks_add_const_vxx_0, 0), (self.blocks_multiply_const_vxx_1, 0))    
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_add_xx_1, 0))    
        self.connect((self.blocks_add_xx_1, 0), (self.uhd_usrp_sink_0, 0))    
        self.connect((self.blocks_float_to_complex_0, 0), (self.blocks_add_xx_0, 0))    
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_add_const_vxx_0, 0))    
        self.connect((self.blocks_multiply_const_vxx_1, 0), (self.blocks_float_to_complex_0, 0))    
        self.connect((self.blocks_multiply_const_vxx_2, 0), (self.blocks_multiply_const_vxx_4, 0))    
        self.connect((self.blocks_multiply_const_vxx_3, 0), (self.blocks_multiply_const_vxx_4_0, 0))    
        self.connect((self.blocks_multiply_const_vxx_4, 0), (self.blocks_add_xx_0, 1))    
        self.connect((self.blocks_multiply_const_vxx_4_0, 0), (self.blocks_add_xx_1, 1))    
        self.connect((self.digital_dxpsk_mod_0, 0), (self.blocks_multiply_const_vxx_2, 0))    
        self.connect((self.digital_gfsk_mod_0, 0), (self.blocks_multiply_const_vxx_3, 0))    
        self.connect((self.rational_resampler_xxx_0, 0), (self.blocks_multiply_const_vxx_0, 0))    
        self.connect((self.uhd_usrp_source_0, 0), (self.wxgui_scopesink2_0, 0))    
        self.connect((self.uhd_usrp_source_0, 0), (self.wxgui_waterfallsink2_0, 0))    


    def get_transmit_power(self):
        return self.transmit_power

    def set_transmit_power(self, transmit_power):
        self.transmit_power = transmit_power
        self._transmit_power_slider.set_value(self.transmit_power)
        self._transmit_power_text_box.set_value(self.transmit_power)
        self.uhd_usrp_sink_0.set_gain(self.transmit_power, 0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_source_0.set_bandwidth(self.samp_rate, 0)
        self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
        self.wxgui_waterfallsink2_0.set_sample_rate(self.samp_rate)

    def get_psk_1on_0off(self):
        return self.psk_1on_0off

    def set_psk_1on_0off(self, psk_1on_0off):
        self.psk_1on_0off = psk_1on_0off
        self._psk_1on_0off_slider.set_value(self.psk_1on_0off)
        self._psk_1on_0off_text_box.set_value(self.psk_1on_0off)
        self.blocks_multiply_const_vxx_4.set_k((self.psk_1on_0off, ))

    def get_modulated_signal_amplitude(self):
        return self.modulated_signal_amplitude

    def set_modulated_signal_amplitude(self, modulated_signal_amplitude):
        self.modulated_signal_amplitude = modulated_signal_amplitude
        self._modulated_signal_amplitude_slider.set_value(self.modulated_signal_amplitude)
        self._modulated_signal_amplitude_text_box.set_value(self.modulated_signal_amplitude)
        self.blocks_multiply_const_vxx_2.set_k((self.modulated_signal_amplitude, ))
        self.blocks_multiply_const_vxx_3.set_k((self.modulated_signal_amplitude, ))
        self.blocks_multiply_const_vxx_0.set_k((self.modulated_signal_amplitude, ))

    def get_modulated_freq(self):
        return self.modulated_freq

    def set_modulated_freq(self, modulated_freq):
        self.modulated_freq = modulated_freq
        self._modulated_freq_slider.set_value(self.modulated_freq)
        self._modulated_freq_text_box.set_value(self.modulated_freq)

    def get_fsk_1on_0off(self):
        return self.fsk_1on_0off

    def set_fsk_1on_0off(self, fsk_1on_0off):
        self.fsk_1on_0off = fsk_1on_0off
        self._fsk_1on_0off_slider.set_value(self.fsk_1on_0off)
        self._fsk_1on_0off_text_box.set_value(self.fsk_1on_0off)
        self.blocks_multiply_const_vxx_4_0.set_k((self.fsk_1on_0off, ))

    def get_carrier_freq(self):
        return self.carrier_freq

    def set_carrier_freq(self, carrier_freq):
        self.carrier_freq = carrier_freq
        self._carrier_freq_slider.set_value(self.carrier_freq)
        self._carrier_freq_text_box.set_value(self.carrier_freq)
        self.uhd_usrp_sink_0.set_center_freq(self.carrier_freq, 0)
        self.uhd_usrp_source_0.set_center_freq(self.carrier_freq, 0)

    def get_am_1on_0off(self):
        return self.am_1on_0off

    def set_am_1on_0off(self, am_1on_0off):
        self.am_1on_0off = am_1on_0off
        self._am_1on_0off_slider.set_value(self.am_1on_0off)
        self._am_1on_0off_text_box.set_value(self.am_1on_0off)
        self.blocks_multiply_const_vxx_1.set_k((self.am_1on_0off, ))


if __name__ == '__main__':
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    (options, args) = parser.parse_args()
    tb = replace_me()
    tb.Start(True)
    tb.Wait()
