import scipy
import wave
from scipy.io import wavfile
import scipy.fftpack
import matplotlib.pyplot as plotlib
from pylab import *
import sys
import numpy
import struct

def getFFT(fName):
    sampRate, readings = wavfile.read(fName)
    #readings = [(elt/2**16)*2-1 for elt in readings]
    ffted = scipy.fftpack.rfft(readings)
    return ffted

def getFFTN(fName, numBuckets):
    sampRate, readings = wavfile.read(fName)
    ffted = scipy.fftpack.rfft(readings, numBuckets)
    return ffted

def saveFFT(ffted, name, figNum):
    print "saving"+name
    print 'type: ', type(ffted[550])
    print "len", len(ffted)
    print 'name: ', name, ffted[500:510]
    halfLen = len(ffted)/2
    plt.figure(figNum)
    plotlib.ylabel("power")
    plotlib.xlabel("frequency")
    lenFFT = len(ffted)
    xVals = range(0, 44100, 44100/lenFFT)
    xAxis = np.array(xVals[:len(xVals)/2-2], dtype='float64')
    print len(xAxis), len(ffted[:halfLen-1])
    plt.plot(xAxis, abs(ffted[:halfLen-1]))
    savefig(name+".png", bbox_inches="tight")

def compFFTs(data1, data2, fftLen):
    sampRate = 44100
    if len(data1) > len(data2) :
        data1 = data1[0:len(data2)]
    elif len(data2) > len(data1) :
        data2 = data2[0:len(data1)]
    fft1 = scipy.fftpack.rfft(data1)
    fft2 = scipy.fftpack.rfft(data2)
    step = sampRate / fftLen 
    diffCoef = []
    for i in range(0, len(fft1), 1):
        diffCoef.append( fft2[i] / fft1[i] )
    inversions = []
    for dC in diffCoef:
        inversions.append(1.0/dC)
    return inversions
      
#infile and outfile must be the same length
def invertTransform(inFile, outFile, invFile):
    sr1, data1 = wavfile.read(inFile)
    dataLen = len(data1)
    sr2, data2 = wavfile.read(outFile)
    fft1 = scipy.fftpack.rfft(data1)
    fft2 = scipy.fftpack.rfft(data2)
    inMean = np.mean(fft1)
    outMean = np.mean(fft2)
    inNorm = np.array(fft1/inMean)
    outNorm = np.array(fft2/outMean)
    nums = []
    for i in range(0, len(fft1), 1):
        if abs(inNorm[i]) > abs(outNorm[i]):
            nums.append(fft1[i]*1.25)
        else:
            nums.append(fft1[i]*.75)
        if nums[i] > (2**15)-1:
            print "capped"
            #nums[i] = (2**15)-1
    adapted = np.array(nums, dtype='float64')
    print 'adapted', adapted[100], len(adapted), type(adapted[0])
    print 'fft1', fft1[100], len(fft1), type(fft1[0])
    print 'fft2', fft2[100], len(fft2), type(fft2[0])
    unfftedAdapted = scipy.fftpack.irfft(adapted)
    casted = unfftedAdapted.astype(int16)
    wavfile.write(invFile, 44100.0, casted)

def createMalleable(inFile, outFile, invFile):
    print "create malleable"
    sr1, data1 = wavfile.read(inFile)
    dataLen = len(data1)
    sr2, data2 = wavfile.read(outFile)
    fft1 = scipy.fftpack.rfft(data1)
    print "one fft done"
    fft2 = scipy.fftpack.rfft(data2)
    nums = []
    step = 44100.0 /  len(fft1)
    freq = 0.0
    for i in range(0, len(fft1), 1):
        if freq < 8000:
            nums.append(fft1[i]*1.25)
        else:
            nums.append(0.0)
        freq = freq + step
    adapted = np.array(nums, dtype='float64')
    print 'adapted', adapted[100], len(adapted), type(adapted[0])
    print 'fft1', fft1[100], len(fft1), type(fft1[0])
    print 'fft2', fft2[100], len(fft2), type(fft2[0])
    unfftedAdapted = scipy.fftpack.irfft(adapted)
    casted = unfftedAdapted.astype(int16)
    scaled = [(elt/2**16.)*2 for elt in casted]
    wavfile.write(invFile, 44100.0, casted)

#first arg is existing file, second is the adapted to be created
def main(args):
    file1 = str(args[1])
    file2 = str(args[2])
    file3 = str(args[3])
    sr1, data1 = wavfile.read(file1)
    sr2, data2 = wavfile.read(file2)
    fftLen = 1024
    #createMalleable(file1, file2, file3)
    invertTransform(file1, file2, file3)
    fft1 = getFFTN(file1, fftLen)
    saveFFT(fft1, file1, 1)
    fft2 = getFFTN(file2, fftLen)
    saveFFT(fft2, file2, 2)
    fft3 = getFFTN(file3, fftLen)
    saveFFT(fft3, file3, 3)

if __name__ == '__main__' : main(sys.argv)





