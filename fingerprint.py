import scipy
from scipy.io import wavfile
import scipy.fftpack
import matplotlib.pyplot as plotlib
from pylab import *
import sys
import numpy
import struct

def getTimeDomainData(fName):
    sampRate, timeDomainData = wavfile.read(fName)
    return timeDomainData

def getFFTN(timeDomainData, numBuckets):
    ffted = scipy.fftpack.rfft(timeDomainData, numBuckets)
    return ffted

def findEnd(ffted):
    maxDiff = 0.0
    end = 1
    for i in range(2, len(ffted)-1, 1):
        preMean = numpy.mean(abs(ffted[:i]))
        postMean = numpy.mean(abs(ffted[i:]))
        diff = abs(postMean-preMean)
        if diff > maxDiff:
            maxDiff = diff
            end = i
    return end

def normalize(ffted, endPoint):
    fftMax = numpy.amax(abs(ffted[0:endPoint]))
    normalized = [abs(elt) / fftMax for elt in ffted]
    return normalized[0:endPoint]

def findDiffs(norm1, norm2):
    length = len(norm1)
    if len(norm2) < length:
        length = len(norm2)
    diffs = []
    for i in range(0, length, 1):
        diffs.append( norm1[i] - norm2[i] )
    return diffs

#the two ffts to be compared should have the same number of buckets 
    #and be made from files of the same length
def compFFTs(ffted1, ffted2):
    fft1Greater = []
    fft2Greater = []
    for i in range(0, len(ffted1), 1):
        if abs(ffted1[i]) > abs(ffted2[i]):
            count = 1
        else:
            print "not greater"

#this function assumes the vals came from a 44100-1024 fft in order
    #to create the x axis vals
def plotDiffs(diffs, name, figNum):
    length = len(diffs)    
    xVals = range(0, 44100, 44100/1024)
    xAxis = np.array(xVals[:len(xVals)/2-2], dtype='float64')
    print len(xAxis[:len(diffs)]), len(diffs)
    plotNums(xAxis[:len(diffs)], diffs, name, figNum)

def plotFFT(ffted, name, figNum):
    lenFFT = len(ffted)
    halfLen = lenFFT/2
    xVals = range(0, 44100, 44100/lenFFT)
    xAxis = np.array(xVals[:len(xVals)/2-2], dtype='float64')
    print len(xAxis), len(ffted[:halfLen-1])
    plotNums(xAxis, abs(ffted[:halfLen-1]), name, figNum)

def plotNums(xVals, yVals, name, figNum):
    print 'lens', len(xVals), len(yVals)
    plt.figure(figNum)
    plotlib.ylabel("power")
    plotlib.xlabel("frequency")
    xAxis = np.array(xVals, dtype='float64')
    yAxis = np.array(yVals, dtype='float64')
    plt.plot(xAxis, yAxis) 
    savefig(name+".png", bbox_inches="tight")

def main(args):
    f1 = args[1]
    f2 = args[2]
    tData1 = getTimeDomainData(f1)
    tData2 = getTimeDomainData(f2)
    numBuckets = 1024
    fft1 = getFFTN(tData1, numBuckets)
    fft2 = getFFTN(tData2, numBuckets)
    plotFFT(fft1,f1 , 1)
    plotFFT(fft2, f2, 2)
    #compFFTs(fft1, fft2)
    fft2End = findEnd(fft2)
    norm2 = normalize(fft2, fft2End)
    norm1 = normalize(fft1, numBuckets)
    print (sum(norm2) / len(norm2))
    print (sum(norm1) / len(norm1))
    #diffs = findDiffs(norm1, norm2)
    #plotDiffs(diffs,f1+f2+"diffs" , 3)

if __name__ == '__main__' : main(sys.argv)


