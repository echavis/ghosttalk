from fingerprint import *

class signalInfo:
    def __init__(self, fft, peakMin):
        self.peaks = peakDetect(fft, peakMin)
        self.harmonicScore = harmonicDetector(fft)
        self.symmetryScore = symmetryDetector(fft)
        self.sharpnessScore = sharpnessDetector(fft)
        self.cm = centerOfMassDetector(fft)
    def makeArray(self):
        infoArray = []
        infoArray.append(len(self.peaks))
        infoArray.append(self.harmonicScore)
        infoArray.append(self.symmetryScore)
        infoArray.append(self.sharpnessScore)
        infoArray.append(self.cm)
        return infoArray

def sub5Peaks(fft):
    minH = .1
    peaks = peakDetect(fft, minH)
    while len(peaks) > 5:
        minH = minH + .05
        peaks = peakDetect(fft, minH)
    return peaks

def harmonicDetector(fft):
    peaks = sub5Peaks(fft)
    sumModulos = 0.0
    for i in range(1, len(peaks), 1):
        sumModulos += peaks[i] % peaks[0]
    return sumModulos

def symmetryDetector(fft):
    peaks = sub5Peaks(fft)
    symmetryScore = getDataSymmetry(fft, peaks)
    return symmetryScore

def getSharpness(fft, peak):
    left = peak - 1
    right = peak + 1
    while fft[left] <= fft[left+1]:
        left = left - 1
    while fft[right] <= fft[right-1]:
        right = right + 1
    return fft[peak] / (right - left)

def sharpnessDetector(fft):
    peaks = sub5Peaks(fft)
    sharpSum = 0.0
    for p in peaks:
        sharpSum += getSharpness(fft, p)
    sharpScore = sharpSum / len(peaks)
    return sharpScore

def centerOfMassDetector(fft):
    total = 0.0
    weighted = 0.0
    for i in range(0, len(fft), 1):
        total += fft[i]
        weighted += i * fft[i]
    cm = weighted / total
    return cm


