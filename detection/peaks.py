import scipy
import numpy
import sys
from fingerprint import *
import csv

def estimatePeakWidth(data, idx):
    leftSide = 0
    rightSide = 0
    while data[idx - leftSide] <= data[idx]:
        leftSide += 1
    while data[idx + rightSide] <= data[idx]:
        rightSide += 1
    if leftSide < rightSide:
        return leftSide
    else:
        return rightSide

def getDiffPointSymScore(data, idx, estPeakWidth):
    diffSum = 0.0
    for i in range(1, estPeakWidth+1, 1):
        diffSum += abs(data[idx-i] - data[idx+i])
    return diffSum

def writeToCSV(ffts, fName):
    with open(fName, 'wb') as csvFile:
        spamwriter = csv.writer(csvFile, delimiter=',')
        for f in ffts:
            spamwriter.writerow(f)
    return 1

def placement(data, minHeight):
    peaks = peakDetect(data, minHeight)
    sumModulos = 0.0
    for i in range(1, len(peaks), 1):
        sumModulos += peaks[0] % peaks[i]
    return sumModulos / (len(peaks)-1)

def getSlope(a, b):
    return b-a

def getSlopePointSymScore(data, idx, estPeakWidth):
    diffSum = 0.0
    for i in range(1, estPeakWidth+1, 1):
        leftSlope = getSlope(data[idx-i], data[idx-i+1])
        rightSlope = getSlope(data[idx+i], data[idx+i-1])
        diffSum += abs(leftSlope-rightSlope)
    return diffSum

def getPeakArea(data, idx):
    rightEnd = idx
    leftEnd = idx
    area = 0.0
    while data[rightEnd] <= data[idx]:
        rightEnd += 1
    while data[leftEnd] <= data[idx]:
        leftEnd -= 1
    secondToRight = rightEnd - 1
    secondToLeft = leftEnd + 1
    leftBottom = data[leftEnd]
    rightBottom = data[rightEnd]
    leftTri = .5 * 1 * (data[secondToLeft] - leftBottom)
    rightTri = .5 * 1 * (data[secondToRight] - rightBottom)
    if secondToRight == secondToLeft:
        return leftTri + rightTri
    cur = secondToLeft+1
    while cur <= secondToRight: 
        print 'function not done'

def fitPolynomialFFT(yVals, degree):
    xVals = range(0, 44100, 44100/len(yVals))
    fittedPoly = numpy.polyfit(xVals[:len(yVals)], yVals, degree)
    return fittedPoly

def findPeakEdges(data, peak):
    left = peak-1
    right = peak+1
    while data[left] <= data[left+1] and left > 0:
        left -= 1
    while data[right] <= data[right-1] and right < len(data)-1:
        right += 1
    print "right left: ", (left, right)
    return (left, right)

def getRangeMass(data, left, right):
    mass = 0.0
    for i in range(left, right+1, 1):
        mass += data[i]
    return mass

def massConcentration(data, minHeight):
    peaks = peakDetect(data, minHeight)
    total = numpy.sum(data)
    peakTotal = 0.0
    for p in peaks:
        leftRightTup = findPeakEdges(data, p)
        peakTotal += getRangeMass(data, leftRightTup[0], leftRightTup[1])
    return peakTotal/total

def massConcentrationPerPeak(data, minHeight):        
    peaks = peakDetect(data, minHeight)
    total = numpy.sum(data)
    peakTotal = 0.0
    for p in peaks:
        leftRightTup = findPeakEdges(data, p)
        peakTotal += getRangeMass(data, leftRightTup[0], leftRightTup[1])
    return (peakTotal/total)/len(peaks)

def main(args):
    fftLen = 1024
    ffts = []
    for x in range(1, len(args), 1):
        ffts.append( getFFTN( getTimeDomainData( args[x] ), fftLen ) )
    print len(ffts)
    for i in range(0, len(args)-1, 1):
        print placement(ffts[i], .2)
        print args[i+1]
        print massConcentrationPerPeak(ffts[i], .2)
        plotWithPeaks(ffts[i], args[i+1], .2, i-1)

if __name__ == '__main__' : main(sys.argv)
