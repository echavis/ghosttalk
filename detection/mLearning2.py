import sys
import fingerprint
from sklearn import svm, datasets
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.decomposition import PCA
from sklearn.decomposition import RandomizedPCA
from sklearn.grid_search import GridSearchCV
import matplotlib.pyplot as plt
import numpy
from scipy.io import wavfile

'''
              !!!  file 1 must start before file 2 !!!
find the match up point by starting at the beginning of file 1 and going to 1/4 of the way through file 1
while looking for the point at which the correlation coefficient is highest in comparison to file 2
'''
def syncTimeDomain(tD1, tD2):
    bestSyncIDX = 0  
    bestSync = 0
    for i in range(0, len(tD1)/4, 10):
        corrCoefOut = numpy.corrcoef( tD1[i:i+100000], tD2[0:100000] )
        syncVal = corrCoefOut[0][1]
        if syncVal > bestSync:
            bestSync = syncVal
            bestSyncIDX = i
    for j in range(bestSyncIDX-10, bestSyncIDX+10, 1):
        corrCoefOut = numpy.corrcoef( tD1[j:j+100000], tD2[0:100000] )
        syncVal = corrCoefOut[0][1]
        if syncVal > bestSync:
            bestSync = syncVal
            bestSyncIDX = j
    if bestSyncIDX < .5:
        print 'WARNING: BEST SYNC IS UNDER .5 CORRELATION COEFFICIENT', bestSync
    return (bestSyncIDX, bestSync)

'''
removes the areas that should be close to silent in a signal
'''
def removeQuiet(tD, tDChunks):
    notEmpty = []
    scale = numpy.max(numpy.absolute(tD))
    for tDC in tDChunks:
        if numpy.max(tDC) > scale/8:
            notEmpty.append(tDC)
    combined = []
    for x in notEmpty:
        combined.extend(x)
    toWrite = numpy.array(combined)
    wavfile.write( "written.wav", 44100, toWrite )
    return notEmpty

'''
removes the need for tD1 to start before tD2
tries it assuming each starts first, picks the better sync
returns syncPoint, file that starts first, file that starts second, then a bool for flipped or not
'''
def getBestSync(tD1, tD2):
    sync1 = syncTimeDomain(tD1, tD2)
    if sync1[1] > .6:
        return (sync1[0], tD1, tD2, False)
    sync2 = syncTimeDomain(tD2, tD1)
    if sync1[1] > sync2[1]:
        return (sync1[0], tD1, tD2, False)
    else:
        return (sync2[0], tD2, tD1, True)

'''
takes the time domain arrays 1 and 2 and splits them to corresponding blocks of length numSamps to do FFTs on, 
starts at sync point for tD1 and at 0 for tD2.
'''
def splitToChunks(syncPoint, tD1, tD2, flipBool, numSamps):
    tD1Chunks = []
    tD2Chunks = []
    toChop1 = tD1[syncPoint:]
    toChop2 = tD2[:len(toChop1)]
    step = numSamps
    i = syncPoint
    while i < len(toChop1)-numSamps:
        toAdd1 = toChop1[i:i+numSamps]
        toAdd2 = toChop2[i:i+numSamps]
        tD1Chunks.append(toAdd1)
        tD2Chunks.append(toAdd2)
        i = i + numSamps
    if flipBool == True:
        return (tD2Chunks, tD1Chunks)
    return (tD1Chunks, tD2Chunks)

'''
normalizes an fft to between -1 and 1
'''
def normFFT(fftIn):
    largest = numpy.max(numpy.absolute(fftIn))
    fftOut = [ x/largest for x in fftIn ]
    return fftOut

'''
get the ffts of all the segments of the time domain data for both files
'''
def getFFTNs(chunks1, chunks2, numBuckets):
    ffts1 = [ fingerprint.getFFTN(x, numBuckets) for x in chunks1]
    ffts2 = [ fingerprint.getFFTN(x, numBuckets) for x in chunks2]
    norm1 = normFFT(ffts1)
    norm2 = normFFT(ffts2)
    return (norm1, norm2)

'''
find the principal components using the scikit learn PCA module
'''
def getPCA(ffts1, ffts2, ):
    X = numpy.array(ffts1) 
    Y = numpy.array(ffts2)
    pca = RandomizedPCA(n_components=3)
    pca.fit(X)
    new_x = pca.transform(X)
    new_y = pca.transform(Y)
    print pca.explained_variance_ratio_
    return (pca, new_x, new_y)

'''
creates a set to do training on from the pca'ed vals of a true and false group of ffts
returns the pca'ed ffts, and a corresponding array of bools
'''
def createTrainingSet(pcaFalse, pcaTrue):
    pcaAll = []
    output = []
    for x in pcaFalse:
        pcaAll.append(x)
        output.append(False)
    for y in pcaTrue:
        pcaAll.append(y)
        output.append(True)
    return (pcaAll, output) 

'''
train an SVM on the PCAd true and false groups
'''
def trainSVM(pcaFalse, pcaTrue, bestParams):
    print 'num trues', len(pcaTrue), 'num falses', len(pcaFalse)
    pcaRes = []
    bools = []
    pcaRes, bools = createTrainingSet(pcaFalse, pcaTrue)
    clf = svm.SVC( C = bestParams['C'], gamma= bestParams['gamma'])
    clf.fit(pcaRes, bools)
    return clf

'''
do the cross validation and select parameters
this is done with the grid search from scikit
returns the best parameters which can then be used to train the actual one
I currently am under the impression that it does the folding itself
    a. num of folds based on the cv variable
    b. I think it tests each of the folds and averages? not sure...
'''
def crossValidate(pcaFalse, pcaTrue, cVals, gammaVals):
    print 'start cross validation'
    tuningParams = [ { 'gamma': gammaVals, 'C': cVals } ]
    clf = GridSearchCV(svm.SVC(), tuningParams, cv=3)
    pcaAll, output = createTrainingSet(pcaFalse, pcaTrue)
    print 'right before fit'
    clf.fit(pcaAll, output)
    print 'best estimator', clf.best_estimator_
    #print clf.grid_scores_
    print 'best params', clf.best_params_
    return clf.best_params_

'''
classify an fft as from an injection or no using an SVM after its principal components are found
'''
def predict( clf, pca, fft ):
    postPCAFFT = pca.transform(fft) 
    return clf.predict(postPCAFFT)   

'''
creates a dataset to do predictions on
takes a file name, the num of fft buckets, and the number of time domain samples to make into each fft
'''
def createSetToPredict( fName, numBuckets, numSamps):
    tD = fingerprint.getTimeDomainData(fName) 
    chunks = []
    i = 0
    while i < len(tD)-numSamps:
        chunks.append(tD[i:i+numSamps] )
        i += numSamps
    normFFTedChunks = [ normFFT(fingerprint.getFFTN(x, numBuckets)) for x in chunks ]
    return normFFTedChunks

def main(args):
    sampRate = 44100
    numBuckets = 64
    tD1 = fingerprint.getTimeDomainData(args[1])
    tD2 = fingerprint.getTimeDomainData(args[2])
    bestSync = getBestSync(tD1, tD2)
    #bestSync[3] tells you if the sync flipped them or not
    print bestSync
    print 'flipped?', bestSync[3]
    chunks = splitToChunks(bestSync[0], bestSync[1], bestSync[2], bestSync[3], 220)
    chunksWithoutQuiet1 = removeQuiet(tD1, chunks[0])
    chunksWithoutQuiet2 = removeQuiet(tD2, chunks[1])
    fftedChunks = getFFTNs(chunksWithoutQuiet1, chunksWithoutQuiet2, numBuckets)
    pcaTup = getPCA(fftedChunks[0], fftedChunks[1])
    gammaVals = cVals = numpy.logspace(-2, 2, 5)
    bestParams = crossValidate(pcaTup[1], pcaTup[2], cVals, gammaVals)
    clf = trainSVM(pcaTup[1], pcaTup[2], bestParams)
    print 'clf', clf
    '''
    toPredict1 = fftedChunks[0][1000]
    toPredict2 = fftedChunks[1][1000]
    prediction1 = predict( clf, pcaTup[0], toPredict1)
    prediction2 = predict( clf, pcaTup[0], toPredict2)
    print prediction1
    print prediction2
    '''
    toPredictAll = createSetToPredict(args[3], numBuckets, 220)
    #print toPredictAll[1000:1010]
    trueCount = 0.0
    for i, x in enumerate(toPredictAll):
        if predict(clf, pcaTup[0], x) == True:
            trueCount += 1
    print 'trueCount', trueCount, 'num', len(toPredictAll), 'trueRate: ', trueCount / len(toPredictAll)
    #print clf
   
    #plotSVM( clf, pcaTup[1], pcaTup[2] )

if __name__ == '__main__' : main(sys.argv)


def plotSVM(clf, pca1Whole, pca2):
    pca1 = pca1Whole[len(pca1Whole)/4-500:len(pca1Whole)/4+500]
    x_min, x_max = pca1[:,0].min() - 1, pca1[:,0].max() + 1
    y_min, y_max = pca1[:,1].min() - 1, pca1[:,1].max() + 1
    h = (x_max-x_min) / 20
    xx, yy = numpy.meshgrid( numpy.arange(x_min, x_max, h), numpy.arange(y_min, y_max, h) )
    plt.subplot(2,2,1)
    plt.subplots_adjust(wspace=0.4, hspace=0.4)
    Z=clf.predict(numpy.c_[xx.ravel(), yy.ravel()])
    Z=Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.Paired, alpha=0.8)
    plt.scatter(pca1[:,0], pca1[:,0], c='y', cmap=plt.cm.Paired)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.xticks(())
    plt.yticks(())
    plt.show()


