import scipy
from scipy.io import wavfile
import scipy.fftpack
import matplotlib.pyplot as plotlib
from pylab import *
import sys
import numpy
import struct
import peakutils
import peakutils.plot
from matplotlib import pyplot
from scipy import signal
from peaks import *

def getTimeDomainData(fName):
    sampRate, timeDomainData = wavfile.read(fName)
    return timeDomainData

def getFFTN(timeDomainData, numBuckets):
    ffted = scipy.fftpack.rfft(timeDomainData, numBuckets)
    return abs(ffted)

def findEnd(ffted):
    maxDiff = 0.0
    end = 1
    for i in range(2, len(ffted)-1, 1):
        preMean = numpy.mean(ffted[:i])
        postMean = numpy.mean(ffted[i:])
        diff = abs(postMean-preMean)
        if diff > maxDiff:
            maxDiff = diff
            end = i
    return end

def normalize(ffted, endPoint):
    fftMax = numpy.amax(ffted[0:endPoint])
    normalized = [elt / fftMax for elt in ffted]
    return normalized[0:endPoint]

def findDiffs(norm1, norm2):
    length = len(norm1)
    if len(norm2) < length:
        length = len(norm2)
    diffs = []
    for i in range(0, length, 1):
        diffs.append( norm1[i] - norm2[i] )
    return diffs

#the two ffts to be compared should have the same number of buckets 
    #and be made from files of the same length
def compFFTs(ffted1, ffted2):
    fft1Greater = []
    fft2Greater = []
    for i in range(0, len(ffted1), 1):
        if ffted1[i] > ffted2[i]:
            count = 1
        else:
            print "not greater"

#this function assumes the vals came from a 44100-1024 fft in order
    #to create the x axis vals
def plotDiffs(diffs, name, figNum):
    length = len(diffs)    
    xVals = range(0, 44100, 44100/1024)
    xAxis = np.array(xVals[:len(xVals)/2-2], dtype='float64')
    print len(xAxis[:len(diffs)]), len(diffs)
    plotNums(xAxis[:len(diffs)], diffs, name, figNum)

def plotFFT(ffted, name, figNum):
    lenFFT = len(ffted)
    halfLen = lenFFT/2
    xVals = range(0, 44100, 44100/lenFFT)
    xAxis = np.array(xVals[:len(xVals)/2-2], dtype='float64')
    print len(xAxis), len(ffted[:halfLen-1])
    yAxis = ffted[:len(xAxis)]
    plotNums(xAxis, yAxis, name, figNum)

def plotWithPeaks(ffted, name, minHeight, figNum):
    peaks = peakDetect(ffted, minHeight)
    lenFFT = len(ffted)
    halfLen = lenFFT/2
    xVals = range(0, 44100, 44100/lenFFT)
    xAxis = np.array(xVals[:len(xVals)/2-2], dtype='float64')
    print len(xAxis), len(ffted[:halfLen-1])
    yAxis = np.array(ffted[:halfLen-1], dtype='float64') 
    print 'len x', len(xAxis)
    print 'len y', len(yAxis)
    if len(xAxis) > len(yAxis):
        xAxis = xAxis[len(yAxis)]
    elif len(yAxis) > len(xAxis):
        yAxis = yAxis[len(xAxis)]
    idx = len(peaks)-1
    while peaks[idx] > len(xAxis) and idx >= 0:
        idx -= 1
    peakutils.plot.plotWithFigNum(xAxis, yAxis, peaks[:idx+1], figNum)
    savefig(name+".png", bbox_inches="tight")

def plotBaseline(ffted, name, minHeight, figNum):
    peaks = peakDetect(ffted, minHeight)
    lenFFT = len(ffted)
    halfLen = lenFFT/2
    xVals = range(0, 44100, 44100/lenFFT)
    xAxis = np.array(xVals[:len(xVals)/2-2], dtype='float64')
    print len(xAxis), len(ffted[:halfLen-1])
    yAxis = np.array(ffted[:halfLen-1], dtype='float64') 
    baseline = peakutils.baseline(yAxis)
    peakutils.plot.plotWithFigNum(xAxis, yAxis, peaks, figNum)
    plt.figure(figNum)
    plt.plot(xAxis, baseline)
    savefig(name+".png", bbox_inches="tight")

def plotNums(xVals, yVals, name, figNum):
    print 'lens', len(xVals), len(yVals)
    plt.figure(figNum)
    plotlib.ylabel("power")
    plotlib.xlabel("frequency")
    xAxis = np.array(xVals, dtype='float64')
    yAxis = np.array(yVals, dtype='float64')
    plt.plot(xAxis, yAxis)
    savefig(name+".png", bbox_inches="tight")

def findPeaks(ffted, size):
    peakIdx = []
    if ffted[0] > ffted[1] and ffted[0] > size:
        peakIdx.append(0)
    for i in range(1, len(ffted)-2, 1):
        if ffted[i] > ffted[i-1] and ffted[i] > ffted[i+1] and ffted[0] > size:
            peakIdx.append(i)
    if ffted[-1] > ffted[-2] and ffted[0] > size:
        peakIdx.append(len(ffted)-1)
    return peakIdx

def peakDetect(ffted, minHeight):
    indexes = peakutils.indexes(ffted, thres=minHeight)
    return indexes

def baselineDetect(ffted, degree, maxIters, toler):
    fftedBaseline = peakutils.baseline.baseline(ffted, degree, maxIters, toler)
    return fftedBaseline

#this works way less well
def peakDetect2(ffted, widths):
    peaks = signal.find_peaks_cwt(ffted, widths)
    return peaks

def findPointSymmetry(data, idx):
    diff1 = abs( data[idx-1] - data[idx+1] )
    diff2 = abs( data[idx-2] - data[idx+2] )
    diff3 = abs( data[idx-3] - data[idx+3] )
    score = diff1 + diff2 + diff3
    return score

def getDataSymmetry(data, idxs):
    sum = 0.0
    count = 0
    for i in idxs:
        sum += findPointSymmetry(data, i)
        count += 1
    return sum/count

def main(args):
    f1 = args[1]
    f2 = args[2]
    tData1 = getTimeDomainData(f1)
    tData2 = getTimeDomainData(f2)
    numBuckets = 1024
    fft1 = getFFTN(tData1, numBuckets)
    fft2 = getFFTN(tData2, numBuckets)
    #plotFFT(fft1, f1, 1)
    #plotFFT(fft2, f2, 2)
    #compFFTs(fft1, fft2)
    fft2End = findEnd(fft2)
    norm2 = normalize(fft2, numBuckets)
    norm1 = normalize(fft1, numBuckets)
    print "means info"
    print (sum(norm2) / len(norm2))
    print (sum(norm1) / len(norm1))
    print "peak detection"
    print peakDetect(norm1, .2)
    print peakDetect(norm2, .2)
    plotWithPeaks(fft1, f1, .2, 1)
    plotWithPeaks(fft2, f2, .2, 2)
    #plotBaseline(norm2, "withBaseline2", .2, 4)
    #plotBaseline(norm1, "withBaseline", .2, 3)
    peak1 = peakDetect(fft1, .2)
    peak2 = peakDetect(fft2, .2)
    print getDataSymmetry(fft1, peak1)
    print getDataSymmetry(fft2, peak2)
    print fitPolynomialFFT(norm1, 3)
    print fitPolynomialFFT(norm2, 3)

if __name__ == '__main__' : main(sys.argv)


