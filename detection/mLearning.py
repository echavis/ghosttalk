import sys
from collections import namedtuple
from fingerprint import *
from sklearn import svm
from sklearn.neighbors.nearest_centroid import NearestCentroid
import numpy
from helpful import *

def mlTest():
    x = [[0,0], [2,2] ]
    y = [0,1]
    clf = svm.SVC()
    clf.fit(x,y)
    print clf.predict([2, 3])

def nearestCentroidTrain(ffts, bools):
    clf = NearestCentroid()
    clf.fit(ffts, bools)
    return clf

def SVMcreateAndTrain(ffts, bools):
    clf = svm.SVC()
    clf.fit(ffts, bools)
    return clf

def predict(clf, fft):
    return clf.predict(fft)

def produceDatasetFFT(names, numFFTBuckets):
    ffts = []
    bools = []
    for n in names:
        freqDomain = getFFTN( getTimeDomainData(n), numFFTBuckets )
        ffts.append(freqDomain)
        if "Output" in n:
            bools.append( True)
        else:
            bools.append( False)
    return(ffts, bools)

def produceDatasetInfo(names, numFFTBuckets, peakMin):
    sInfos = []
    bools = []
    for n in names:
        freqDomain = getFFTN( getTimeDomainData(n), numFFTBuckets)
        sInfo = signalInfo(freqDomain, peakMin)
        sInfos.append(sInfo.makeArray())
        if "Output" in n:
            bools.append(True)
        else:
            bools.append(False)
    return (sInfos, bools)

def producePredictInfo(name, numFFTBuckets, peakMin):
    sInfo = None
    isInjected = True
    freqDomain = getFFTN( getTimeDomainData(name), numFFTBuckets)
    sInfo = signalInfo(freqDomain, peakMin)
    dataArray = sInfo.makeArray()
    if "Output" in name:
        isInjected = True
    else:
        isInjected = False 
    return (dataArray, isInjected)

def getMinMax(dataset, idx):
    highest = 0.0
    lowest = dataset[0][0][idx]
    for d in dataset[0]:
        if d[idx] > highest:
            highest = d[idx]
        if d[idx] < lowest:
            lowest = d[idx]
    return (lowest, highest)

def getMaxDifferentiator(dataset, idx):
    thisIdxNum = [ x[idx] for x in dataset[0] ]
    thisIdxBool = [ x for x in dataset[1] ]            
    minMaxTup = getMinMax(dataset, idx)
    bestDiff = 0.0
    bestCutoff = minMaxTup[0]
    cutoff = minMaxTup[0]
    step = (minMaxTup[1] - minMaxTup[0]) / 100.0
    overTrue = True
    while cutoff < minMaxTup[1]:
        oT = True
        oTuF = 0.0
        oFuT = 0.0
        for i in range(0, len(thisIdxNum), 1):
            if thisIdxNum[i] < cutoff and thisIdxBool[i] == True:
                oFuT += 1
            elif thisIdxNum[i] < cutoff and thisIdxBool[i] == False:
                oTuF += 1
            elif thisIdxNum[i] >= cutoff and thisIdxBool[i] == True:
                oTuF += 1
            elif thisIdxNum[i] >= cutoff and thisIdxBool[i] == False:
                oFuT += 1
        L = len(thisIdxNum)          
        perc = oTuF/L if oTuF > oFuT else oFuT/L 
        oT = True if oTuF > oFuT else False
        if perc > bestDiff:
            bestDiff = perc
            bestCutoff = cutoff
            overTrue = oT
        cutoff += step
    bayesTup = getBayesInfo2(thisIdxNum, thisIdxBool, bestCutoff)
    return (bestCutoff, overTrue, bayesTup[0], bayesTup[1], bayesTup[2])

def getBayesInfo(nums, bools, cutoff):
    overCount = 0.0
    trueOverCount = 0.0
    trueCount = 0.0
    for i in range(0, len(nums), 1):
        if nums[i] > cutoff:
            overCount += 1
        if bools[i] == True:
            trueCount += 1
            if nums[i] > cutoff:
                trueOverCount += 1
    #prob true when over
    trueOverRate = trueOverCount / overCount
    #prob true 
    trueRate = trueCount / len(nums)
    #prob over
    probOver = overCount / len(nums)
    #prob over when true
    probOverWhenTrue = trueOverCount / trueCount
    return (trueRate, trueOverRate, probOverWhenTrue, probOver)

def getBayesInfo2(nums, bools, cutoff):
    trueOverCount = 0.0
    trueUnderCount = 0.0
    falseOverCount = 0.0
    falseUnderCount = 0.0
    for i in range(0, len(nums), 1): 
        if bools[i] == True and nums[i] >= cutoff:
            trueOverCount += 1
        elif bools[i] == True and nums[i] < cutoff:
            trueUnderCount += 1
        elif bools[i] == False and nums[i] >= cutoff:
            falseOverCount += 1
        elif bools[i] == False and nums[i] < cutoff:
            falseUnderCount += 1
    trueCount = trueOverCount + trueUnderCount
    falseCount = falseOverCount + falseUnderCount
    overCount = trueOverCount + falseOverCount
    underCount = trueUnderCount + falseUnderCount
    probOverGivenTrue = trueOverCount / trueCount  
    probOverGivenFalse = falseOverCount / falseCount
    trueProb = trueCount / len(nums)
    bTup = (trueProb, probOverGivenTrue, probOverGivenFalse)
    return bTup

def getPredictionNums(dataset):
    bayesTups = [None]*5
    for i in range(0, 5, 1):
        bayesTups[i] = getMaxDifferentiator(dataset, i)
    return bayesTups       

#infoNums must be the same length as bayesTups, or start with the 
    #characteristics that are included in bayeTups 
def bayesPredict(bayesTups, infoNums ):
    print 'infoNums', infoNums
    prob = 1.0
    for i in range(0, len(bayesTups), 1):
        print 'prob over when true', bayesTups[i][4]
        print 'probOver', bayesTups[i][5]
        if infoNums[i] >= bayesTups[i][0]:
            prob = prob * bayesTups[i][4]
            prob = prob / bayesTups[i][5]
        else:
            prob = prob * (1-bayesTups[i][4])
            prob = prob / (1-bayesTups[i][5])
    prob = prob * bayesTups[i][2]
    return prob

def bayesPredict2(bayesTups, infoNums ):
    numer1 = 1.0
    denom1 = denom2 = 1.0
    idx = 0
    for bT in bayesTups:
        cutoff = bT[0]
        if infoNums[idx] >= cutoff:
            numer1 = numer1 * bT[3]
            denom1 = denom1 * bT[3]
            denom2 = denom2 * bT[4]
        else:
            numer1 = numer1 * (1-bT[3])
            denom1 = denom1 * (1 - bT[3])
            denom2 = denom2 * (1-bT[4])
        idx += 1
    numer = numer1 * bayesTups[0][2]
    denom = denom1 * bayesTups[0][2] + denom2 * (1-bayesTups[0][2])
    return numer/denom

def main(args):
    fftLen = 1024
    #arrayTup = produceDatasetFFT(args[1:-1], fftLen)
    dataset = produceDatasetInfo(args[1:-1], fftLen, .2)
    #toPredict = produceDatasetInfo(args[-1], fftLen, .2)
    toPredict = producePredictInfo(args[-1], fftLen, .2)
    print getPredictionNums(dataset)
    print bayesPredict2(getPredictionNums(dataset), toPredict[0])
    #print whichTrait(dataset)
    '''
    mySVM = SVMcreateAndTrain(arrayTup[0], arrayTup[1])
    mySVM2 = nearestCentroidTrain(arrayTup[0], arrayTup[1])
    mySVM2.shrink_threshold=.5
    print arrayTup[1]
    print mySVM
    print mySVM2
    toPredict = signalInfo(getFFTN( getTimeDomainData(args[-1]), fftLen) , .2)
    #print predict(mySVM, getFFTN( getTimeDomainData(args[-1]), fftLen) )
    #print predict(mySVM2, getFFTN( getTimeDomainData(args[-1]), fftLen) )
    print toPredict.makeArray()
    print predict(mySVM, toPredict.makeArray())
    print predict(mySVM2, toPredict.makeArray())
    '''

if __name__ == '__main__' : main(sys.argv)

def findMaxDifferentiator(dataset, idx):
    highest = 0.0
    lowest = dataset[0][0][idx]
    for d in dataset[0]:
        if d[idx] > highest:
            highest = d[idx]
        if d[idx] < lowest:
            lowest = d[idx]
    best = 0.0
    bestCutoff = lowest
    cutoff = lowest
    step = (highest - lowest) / 10
    overTrue = True
    while cutoff < highest:
        oT = True
        overTrue = 0.0
        overFalse = 0.0
        underTrue = 0.0
        underFalse = 0.0
        cur = 0
        for d in dataset[0]:
            if d[idx] >= cutoff and dataset[1][cur] == True:
                overTrue += 1
            elif d[idx] >= cutoff and dataset[1][cur] == False:
                overFalse += 1
            elif d[idx] < cutoff and dataset[1][cur] == True:
                underTrue += 1
            elif d[idx] < cutoff and dataset[1][cur] == False:
                underFalse += 1
            cur += 1
        option1 = overTrue + underFalse
        option2 = overFalse + underTrue
        L = len(dataset[0])
        perc = option1 / L if option1 > option2 else option2 / L
        oT = True if option1 > option2 else False
        if perc > best:
            best = perc
            bestCutoff = cutoff
            overTrue = oT
        cutoff += step
    return (bestCutoff, best, oT)

def whichTrait(dataset):
    maxDifferentiators = [0.0]*5
    differentiatedRatio = [0.0]*5 
    overTrues = [True]*5
    for i in range(0,5,1):
        tup = findMaxDifferentiator(dataset, i)
        maxDifferentiators[i] = tup[0]
        differentiatedRatio[i] = tup[1]
        overTrues[i] = tup[2]
    return (maxDifferentiators, differentiatedRatio, overTrues)
