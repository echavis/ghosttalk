#!/usr/bin/env python2
##################################################
# GNU Radio Python Flow Graph
# Title: Adc Demod
# Generated: Fri May 15 12:00:40 2015
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from gnuradio import analog
from gnuradio import audio
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class adc_demod(grc_wxgui.top_block_gui):

    def __init__(self):
        grc_wxgui.top_block_gui.__init__(self, title="Adc Demod")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 250000
        self.freq = freq = 900000000

        ##################################################
        # Blocks
        ##################################################
        self.rational_resampler_xxx_3 = filter.rational_resampler_fff(
                interpolation=192000,
                decimation=100000000/2,
                taps=None,
                fractional_bw=None,
        )
        self.rational_resampler_xxx_2 = filter.rational_resampler_fff(
                interpolation=50000000,
                decimation=100000000,
                taps=None,
                fractional_bw=None,
        )
        self.rational_resampler_xxx_1 = filter.rational_resampler_fff(
                interpolation=100000000,
                decimation=900000000,
                taps=None,
                fractional_bw=None,
        )
        self.rational_resampler_xxx_0 = filter.rational_resampler_fff(
                interpolation=250000,
                decimation=44100,
                taps=None,
                fractional_bw=None,
        )
        self.blocks_multiply_xx_0 = blocks.multiply_vff(1)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vff((100000000, ))
        self.beethoven5th = blocks.wavfile_source("/home/echavis/Desktop/beethovens5th.wav", True)
        self.audio_sink_0 = audio.sink(192000, "", True)
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_COS_WAVE, freq, 1, 0)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))    
        self.connect((self.beethoven5th, 0), (self.rational_resampler_xxx_0, 0))    
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.audio_sink_0, 0))    
        self.connect((self.blocks_multiply_xx_0, 0), (self.rational_resampler_xxx_1, 0))    
        self.connect((self.rational_resampler_xxx_0, 0), (self.blocks_multiply_xx_0, 0))    
        self.connect((self.rational_resampler_xxx_1, 0), (self.rational_resampler_xxx_2, 0))    
        self.connect((self.rational_resampler_xxx_2, 0), (self.rational_resampler_xxx_3, 0))    
        self.connect((self.rational_resampler_xxx_3, 0), (self.blocks_multiply_const_vxx_0, 0))    


    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.analog_sig_source_x_0.set_frequency(self.freq)


if __name__ == '__main__':
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    (options, args) = parser.parse_args()
    tb = adc_demod()
    tb.Start(True)
    tb.Wait()
