#!/bin/bash

i="881900000"
while [ $i -lt 882000000 ]
do
        echo $i
        sudo timeout --foreground 10s sudo uhd_siggen -a type=usrp1 -f $i --amplitude=1 -g 1
        i=$[$i+5000]
done
